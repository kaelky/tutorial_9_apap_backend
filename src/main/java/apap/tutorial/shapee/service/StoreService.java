package apap.tutorial.shapee.service;

import apap.tutorial.shapee.model.StoreModel;

import java.util.List;
import java.util.Optional;

public interface StoreService {
    //Method untuk mendapatkan data store berdasarkan id
    Optional<StoreModel> getStoreById(Long id);

    //Method untuk menambah store
    void addStore(StoreModel storeModel);

    //Method untuk mendapatkan data semua store yang tersimpan
    List<StoreModel> getStoreList();

    //Method untuk mengubah data store
    StoreModel changeStore(StoreModel storeModel);

    boolean deleteStore(StoreModel storeTarget);
}

package apap.tutorial.shapee.service;

import apap.tutorial.shapee.model.StoreModel;
import apap.tutorial.shapee.repository.StoreDb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@Transactional
public class StoreRestServiceImpl implements StoreRestService {

    @Autowired
    private StoreDb storeDb;

    @Override
    public StoreModel createStore(StoreModel store) {
        return storeDb.save(store);
    }

    @Override
    public List<StoreModel> retrieveListStoreModel() {
        return storeDb.findAllByOrderByNamaAsc();
    }

    @Override
    public StoreModel getStoreByIdStore(Long idStore) {
        Optional<StoreModel> store = storeDb.findById(idStore);
        if (store.isPresent()) {
            return store.get();
        }
        throw new NoSuchElementException();
    }

    @Override
    public StoreModel changeStore(Long idStore, StoreModel storeModel) {
        StoreModel store = getStoreByIdStore(idStore);
        store.setNama(storeModel.getNama());
        store.setKeterangan(storeModel.getKeterangan());
        store.setFollowers(storeModel.getFollowers());
        return storeDb.save(store);
    }

    @Override
    public void deleteStore(Long idStore){
        StoreModel store = getStoreByIdStore(idStore);
        if (store.getListProduct().size() == 0) {
            storeDb.delete(store);
        } else {
            throw new UnsupportedOperationException();
        }
    }
}
